# DAMOGO BackEnd Test

## Persoalan

 - Buatlah sebuah API (REST/GRAPHQL) meliputi :

   1. Healthcheck `GET /healtcheck` dengan kembalian body kosong
   2. Login `POST /auth/login` menggunakan JWT
   3. CRUD Produk 
   4. Transaksi untuk pembelian produk sesuai qty
   5. Menampilkan transaksi detail beserta daftar pengguna

 - Merancang database untuk kebutuhan API
 - Menyiapkan API dokumentasi, postman / swagger / Graphql Playground

## Syarat

-  Menggunakan Bahasa pemrograman Rust / Golang / Nodejs. Nilai plus jika menggunakan Rust

- Menggunakan Databse MySQL dan menyertakan ERD

- Bebas menerapkan arsitektur / skeleton sesuai standar masing-masing, kerapihan kode akan menjadi faktor utama penilaian

- Nilai plus jika hasil test dikemas kedalam docker compose sehingga projek dapat dijalankan dengan mudah di berbagai device.

- (tanpa docker) kumpulkan hasil file .sql kedalam projek

- Jelaskan secara rinci cara menjalankan projek di dalam readme
## Pengumpulan

Waktu pengerjaan test maksimal 7 hari sejak test ini diberikan, dikumpulkan dalam bentuk publik repositori menggunakan github / gitlab. 


Selamat mengerjakan :)
